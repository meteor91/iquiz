//
//  QuicViewController.swift
//  iquiz
//
//  Created by Билал Сиразитдинов on 27.07.17.
//  Copyright © 2017 Билал Сиразитдинов. All rights reserved.
//

import UIKit

class QuizViewController: UIViewController {

    @IBOutlet weak var correctAnswerLabel: UILabel!
    
    
    @IBOutlet weak var questionLabel: UILabel!
    
    
    @IBOutlet weak var buttonA: UIButton!
    
    @IBOutlet weak var buttonB: UIButton!
    
    @IBOutlet weak var buttonC: UIButton!
    
    @IBOutlet weak var buttonD: UIButton!

    @IBAction func buttonATap(_ sender: Any) {
        selectAnswer(answer: "AnswerA")
    }

    @IBAction func buttonBTap(_ sender: Any) {
        selectAnswer(answer: "AnswerB")
    }

    @IBAction func buttonCTab(_ sender: Any) {
        selectAnswer(answer: "AnswerC")
    }
    
    @IBAction func buttonDTab(_ sender: Any) {
        selectAnswer(answer: "AnswerD")
    }
    
    func nextQestion() {
        if currentQuestionIndex < questions.count - 1 {
            currentQuestionIndex = currentQuestionIndex + 1
            updateUIElements()
        } else {
            let alert = UIAlertController.init(
                title: "Это конец",
                message: "Вы набрали \(rightAnswersCount)  из \(questions.count)",
                preferredStyle: .alert
            )
            let okAction = UIAlertAction.init(
                title: "Ok",
                style: .default,
                handler: {action in self.navigationController?.popViewController(animated: true)}
            )
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func selectAnswer(answer: String) {
        let question = questions[currentQuestionIndex]
        let correctAnswer = question["Correct"]
        
        if answer == correctAnswer {
            rightAnswersCount = rightAnswersCount + 1
            correctAnswerLabel.text = "\(rightAnswersCount)"
        }
        nextQestion()
    }

    
    let questions: Array = [
        [
            "QuestionTitle": "Какой язык программирования создала Apple",
            "AnswerA": "Swift",
            "AnswerB": "C++",
            "AnswerC": "Java",
            "AnswerD": "Kotlin",
            "Correct": "AnswerA"
        ], [
            "QuestionTitle": "Самый удобный банк для предпринимателей",
            "AnswerA": "Точка",
            "AnswerB": "Сбербанк-бизнес",
            "AnswerC": "Модульбанк",
            "AnswerD": "Альфа-бизнес",
            "Correct": "AnswerB"
        ], [
            "QuestionTitle": "Каким программистом стать?",
            "AnswerA": "Android",
            "AnswerB": "Web",
            "AnswerC": "Backend",
            "AnswerD": "IOS",
            "Correct": "AnswerD"
        ], [
            "QuestionTitle": "Какой ios выйдет этой осенью?",
            "AnswerA": "ios 10",
            "AnswerB": "ios 11",
            "AnswerC": "ios 15",
            "AnswerD": "ios 20",
            "Correct": "AnswerB"
        ]
    ]
    
    var rightAnswersCount: Int = 0;
    var currentQuestionIndex: Int = 0;

    override func viewDidLoad() {
        super.viewDidLoad()
        updateUIElements()
    }

    func updateUIElements() {
        let question = questions[currentQuestionIndex]
        
        questionLabel.text = question["QuestionTitle"]
        buttonA.setTitle(question["AnswerA"], for: .normal)
        buttonB.setTitle(question["AnswerB"], for: .normal)
        buttonC.setTitle(question["AnswerC"], for: .normal)
        buttonD.setTitle(question["AnswerD"], for: .normal)
    }





}
